#!/usr/bin/env python3
## coding: UTF-8

import cv2
import numpy as np
import os
from pydub import AudioSegment

import rospy
import message_filters
from sensor_msgs.msg import CompressedImage
from audio_common_msgs.msg import AudioDataStamped

QUE_SIZE = 10 # メッセージのキューのサイズ. 同期される前にキューに保存されるメッセージの最大数.
RATE = 16000 # サンプリングレート
FPS = 30 # フレームレート
SLOP = 0.1 # タイムスタンプの遅れ (0.1ならタイムスタンプが0.1秒以内なら同期)
CHANNELS = 1 # 1はモノラル、2はステレオ


class ExtractAudioVideoFromRosbag:
    def __init__(self, 
    audio_topic_name="/audio/audio_stamped",
    image_topic_name="/hsrb/head_rgbd_sensor/rgb/image_rect_color/compressed"):

        self.temp_image_path = "../data/images"

        # 画像および音声データを保存するためのディレクトリを作成
        os.makedirs(self.temp_image_path, exist_ok=True)

        # 画像および音声トピックを購読
        image_sub = message_filters.Subscriber(image_topic_name, CompressedImage)
        audio_sub = message_filters.Subscriber(audio_topic_name, AudioDataStamped)

        # タイムスタンプの遅れを考慮
        ats = message_filters.ApproximateTimeSynchronizer([image_sub, audio_sub], QUE_SIZE, SLOP)
        ats.registerCallback(self._callback)

        self.audio_segments = []
        self._counter = 0
    
    # Instance Method (Private)
    def _callback(self, image, audio):
        print("callback")
        np_arr = np.frombuffer(image.data, np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        cv2.imwrite(self.temp_image_path + f'/{self._counter}.jpg', image_np)
        
        audio_segment = ExtractAudioVideoFromRosbag._convertion_to_mp3_format_message(audio.audio.data)
        
        self.audio_segments.append(audio_segment)
        self._counter += 1
    
    # Static Method (Private)
    @staticmethod
    def _convertion_to_mp3_format_message(data):
        audio_data = np.frombuffer(data, dtype=np.int16)
        audio_segment = AudioSegment(
            audio_data.tobytes(), 
            frame_rate=RATE,
            sample_width=audio_data.dtype.itemsize,
            channels=CHANNELS
        )

        return audio_segment

if __name__ == '__main__':
    rospy.init_node('extract_audio_video')
    extract_audio_video = ExtractAudioVideoFromRosbag()
    rospy.spin()

    # 画像ファイルを結合し、MP4ファイルに変換
    os.system(f'ffmpeg -r {FPS} -i {extract_audio_video.temp_image_path}/%d.jpg -vcodec libx264 -pix_fmt yuv420p ../data/video.mp4')

    # 音声ファイルを結合し、MP3ファイルに変換
    combined = sum(extract_audio_video.audio_segments, AudioSegment.empty())
    combined.export("../data/sound.mp3", format="mp3")

    # 保存したMP3ファイルをWAVファイルに変換
    audio = AudioSegment.from_mp3("../data/sound.mp3")
    audio.export("../data/sound.wav", format="wav")

    # 一時ファイルを削除
    os.system(f'rm -r {extract_audio_video.temp_image_path}')