#!/usr/bin/env python3
## coding: UTF-8

import rosbag

from pydub import AudioSegment
import numpy as np

class ConversionFromRosbagToAudio:
    def __init__(self, audio_topic_name="/audio/audio"):
        input_path = "../data/raw/sound.bag"
        self._output_path = "../data/result/sound.mp3"
        self._bag = rosbag.Bag(input_path)
        self._audio_data = []
        self._audio_topic_name = audio_topic_name
        self._main()
        
    # Instance Method (Private)
    def _main(self):
        for topic, msg, t in self._bag.read_messages(topics=self._audio_topic_name):
            self._audio_data.extend(msg.data)

        with open(self._output_path, 'wb') as f:
            f.write(bytearray(self._audio_data))
        
if __name__ == '__main__':
    conv_bag2audio = ConversionFromRosbagToAudio()



