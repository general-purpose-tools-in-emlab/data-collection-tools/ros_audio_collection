#!/usr/bin/env python3
## coding: UTF-8

import rospy
from audio_common_msgs.msg import AudioData

class AudioPub:
    def __init__(self, audio_topic_name="/audio"):
        self._frames = []
        file_path = "../data/sound.mp3"
        file = open(file_path, "rb")
        self._frames = file.read()
        file.close()

        self._audio_pub = rospy.Publisher(audio_topic_name, AudioData, queue_size=1)
        self._r = rospy.Rate(10)
        self._main()

    # Instance Method (Private)
    def _main(self):
        while not rospy.is_shutdown():
            self._audio_pub.publish(self._frames)
            rospy.sleep(5)
            self._r.sleep()

if __name__ == '__main__':
    rospy.init_node('audio_publisher', anonymous=True)
    audio_pub = AudioPub()
    


