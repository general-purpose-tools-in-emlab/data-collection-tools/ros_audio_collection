# 'ros_audio_collection' Package
This ROS package is collection program about ros message and sound files.  

*   Maintainer: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).
*   Author: Shoichi Hasegawa ([hasegawa.shoichi@em.ci.ritsumei.ac.jp](mailto:hasegawa.shoichi@em.ci.ritsumei.ac.jp)).

**Content:**

*   [Setup](#Setup)
*   [Launch](#launch)
*   [Files](#files)

## Setup
- `sudo apt install -y ffmpeg`  
- `pip install -r requirements.txt`  

## Launch
- `rosrun ros_audio_collection audio_pub.py`  
(`roslaunch ros_audio_collection audio_pub.launch`)  

- `rosrun ros_audio_collection conversion_from_rosbag_to_audio.py`  
(`roslaunch ros_audio_collection conversion_from_rosbag_to_audio.launch`)  

- `rosrun ros_audio_collection extract_audio_video_from_bag.py`

## Files
- `audio_pub.py`: Publish AudioData message of ROS after converting sound files.  
- `conversion_from_bag_to_audio.py`: Convert from rosbag files to sound files.  
- `extract_audio_video_from_bag.py`: Extract sound and video file from rosbag.  

